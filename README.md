Commerce From Price
===================

This module allows for products in a drupal commerce website to display a 'from'
or 'starting at' price when the product has multiple variations with different
prices. The lowest variation price will be displayed. Note that your product
overview should display commerce products instead of product variations for this
to work.

Please report bugs in the [issue queue](https://www.drupal.org/project/issues/commerce_from_price).


## Requirements

[Drupal commerce](https://www.drupal.org/project/commerce)

## Installation

Install as usual, see http://drupal.org/node/1897420 for further information.

## Usage

The formatters of this module are meant to be used on product level, not on
product varation level. More precisely on the entity reference field referencing
purchasable entities such as **product variations**.

* Enabling the module
* Go to a product's manage display page and alter the field formatter of the
  Variations field to one of the 'from' price formatters:
  * Plain 'from' price
  * Default 'from' price _(most common)_
  * Calculated 'from' price
* Save the product display and clear Drupal cache.
* More information on the plain, default and calculated formatters can be found
  [here](https://docs.drupalcommerce.org/commerce2/developer-guide/pricing/displaying-prices).

## Maintainers

* Lennart Van Vaerenbergh (Fernly) - https://www.drupal.org/u/fernly
