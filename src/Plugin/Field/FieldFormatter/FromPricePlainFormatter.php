<?php

namespace Drupal\commerce_from_price\Plugin\Field\FieldFormatter;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Plugin\Field\FieldFormatter\PricePlainFormatter;

/**
 * Plugin implementation of the 'commerce_from_price_plain' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_from_price_plain",
 *   label = @Translation("Plain 'from' price"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class FromPricePlainFormatter extends PricePlainFormatter {

  use FromPriceFormatterTrait;

  /**
   * {@inheritdoc}
   */
  public function getPriceElement(PurchasableEntityInterface $entity, array $cache_entities): array {
    $price = $entity->getPrice();
    $currencies = $this->currencyStorage->loadMultiple([$price->getCurrencyCode()]);

    return [
      '#theme' => 'commerce_price_plain',
      '#number' => $price->getNumber(),
      '#currency' => $currencies[$price->getCurrencyCode()],
      '#cache' => [
        'contexts' => $this->getCacheContexts($cache_entities),
        'tags' => $this->getCacheTags($cache_entities),
      ],
    ];
  }

}
