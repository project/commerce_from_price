<?php

namespace Drupal\commerce_from_price\Plugin\Field\FieldFormatter;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Plugin\Field\FieldFormatter\PriceCalculatedFormatter;
use Drupal\commerce_price\Price;

/**
 * Plugin implementation of the 'commerce_from_price_calculated' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_from_price_calculated",
 *   label = @Translation("Calculated 'from' price"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class FromPriceCalculatedFormatter extends PriceCalculatedFormatter {

  use FromPriceFormatterTrait;

  /**
   * {@inheritdoc}
   */
  public function getResolvedPrice(PurchasableEntityInterface $entity): Price {
    $context = new Context($this->currentUser, $this->currentStore->getStore(), NULL, [
      'field_name' => $entity->get('price')->getName(),
    ]);

    return $this->chainPriceResolver->resolve($entity, 1, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function getPriceElement(PurchasableEntityInterface $entity, array $cache_entities): array {
    $price = $entity->getPrice();

    return [
      '#theme' => 'commerce_price_calculated',
      '#calculated_price' => $this->currencyFormatter->format(
        $price->getNumber(),
        $price->getCurrencyCode(),
        $this->getFormattingOptions()
      ),
      '#purchasable_entity' => $entity,
      '#cache' => [
        'tags' => $this->getCacheTags($cache_entities),
        'contexts' => $this->getCacheContexts($cache_entities),
      ],
    ];
  }

}
