<?php

namespace Drupal\commerce_from_price\Plugin\Field\FieldFormatter;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Plugin\Field\FieldFormatter\PriceDefaultFormatter;

/**
 * Plugin implementation of the 'commerce_from_price_default' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_from_price_default",
 *   label = @Translation("Default 'from' price"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class FromPriceDefaultFormatter extends PriceDefaultFormatter {

  use FromPriceFormatterTrait;

  /**
   * {@inheritdoc}
   */
  public function getPriceElement(PurchasableEntityInterface $entity, array $cache_entities): array {
    $price = $entity->getPrice();

    return [
      '#markup' =>
      $this->currencyFormatter->format(
        $price->getNumber(),
        $price->getCurrencyCode(),
        $this->getFormattingOptions()
      ),
    ];
  }

}
