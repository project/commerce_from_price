<?php

namespace Drupal\commerce_from_price\Plugin\Field\FieldFormatter;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\Plugin\Field\FieldFormatter\PriceCalculatedFormatter;
use Drupal\commerce_price\Price;

/**
 * Alternative implementation of the 'commerce_from_price_calculated' formatter.
 *
 * This replaces the original commerce_from_price_calculated class when
 * commerce_order module is installed.
 */
class OrderFromPriceCalculatedFormatter extends PriceCalculatedFormatter {

  use FromPriceFormatterTrait;

  /**
   * {@inheritdoc}
   */
  public function getResolvedPrice(PurchasableEntityInterface $entity): Price {
    $context = new Context($this->currentUser, $this->currentStore->getStore(), NULL, [
      'field_name' => $entity->get('price')->getName(),
    ]);

    $adjustment_types = array_filter($this->getSetting('adjustment_types'));
    $result = $this->priceCalculator->calculate($entity, 1, $context, $adjustment_types);

    return $result->getCalculatedPrice();
  }

  /**
   * {@inheritdoc}
   */
  public function getPriceElement(PurchasableEntityInterface $entity, array $cache_entities): array {
    $context = new Context($this->currentUser, $this->currentStore->getStore(), NULL, [
      'field_name' => $entity->get('price')->getName(),
    ]);
    $adjustment_types = array_filter($this->getSetting('adjustment_types'));
    $result = $this->priceCalculator->calculate($entity, 1, $context, $adjustment_types);
    $calculated_price = $result->getCalculatedPrice();

    return [
      '#theme' => 'commerce_price_calculated',
      '#result' => $result,
      '#calculated_price' => $this->currencyFormatter->format($calculated_price->getNumber(), $calculated_price->getCurrencyCode(), $this->getFormattingOptions()),
      '#base_price' => $this->currencyFormatter->format($result->getBasePrice()->getNumber(), $calculated_price->getCurrencyCode(), $this->getFormattingOptions()),
      '#adjustments' => $result->getAdjustments(),
      '#purchasable_entity' => $entity,
      '#cache' => [
        'tags' => $this->getCacheTags($cache_entities),
        'contexts' => $this->getCacheContexts($cache_entities),
      ],
    ];
  }

}
