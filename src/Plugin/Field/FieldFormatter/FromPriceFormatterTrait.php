<?php

namespace Drupal\commerce_from_price\Plugin\Field\FieldFormatter;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Trait containing logic for all 'commerce_from_price' formatters.
 */
trait FromPriceFormatterTrait {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'single_entity' => [
        'label_before' => '',
        'label_after' => '',
      ],
      'multiple_entities' => [
        'label_before' => new TranslatableMarkup('Starting at'),
        'label_after' => '',
      ],
      'process_equal_prices_as_single' => 1,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['process_equal_prices_as_single'] = [
      '#type' => 'checkbox',
      '#title' => new TranslatableMarkup('Consider purchasable entities with equal prices as single purchasable entity'),
      '#default_value' => $this->getSetting('process_equal_prices_as_single'),
    ];

    $elements['single_entity'] = [
      '#type' => 'details',
      '#title' => new TranslatableMarkup('Single purchasable entity'),
    ];

    $elements['single_entity']['label_before'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Label before'),
      '#default_value' => $this->getSetting('single_entity')['label_before'],
      '#description' => new TranslatableMarkup('Label displayed before the price.'),
    ];

    $elements['single_entity']['label_after'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Label after'),
      '#default_value' => $this->getSetting('single_entity')['label_after'],
      '#description' => new TranslatableMarkup('Label displayed after the price.'),
    ];

    $elements['multiple_entities'] = [
      '#type' => 'details',
      '#title' => new TranslatableMarkup('Multiple purchasable entities'),
    ];

    $elements['multiple_entities']['label_before'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Label before'),
      '#default_value' => $this->getSetting('multiple_entities')['label_before'],
      '#description' => new TranslatableMarkup('Label displayed before the price.'),
    ];

    $elements['multiple_entities']['label_after'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Label after'),
      '#default_value' => $this->getSetting('multiple_entities')['label_after'],
      '#description' => new TranslatableMarkup('Label displayed after the price.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $summary[] = new TranslatableMarkup('Consider equal prices as single entity price: @value', [
      '@value' => $this->getSetting('process_equal_prices_as_single'),
    ]);

    $summary[] = Markup::create('<strong>' . new TranslatableMarkup('Single purchasable entity') . '</strong>');

    $summary[] = new TranslatableMarkup('Label before: @value', [
      '@value' => $this->getSetting('single_entity')['label_before'],
    ]);

    $summary[] = new TranslatableMarkup('Label after: @value', [
      '@value' => $this->getSetting('single_entity')['label_after'],
    ]);

    $summary[] = Markup::create('<strong>' . new TranslatableMarkup('Multiple purchasable entities') . '</strong>');

    $summary[] = new TranslatableMarkup('Label before: @value', [
      '@value' => $this->getSetting('multiple_entities')['label_before'],
    ]);

    $summary[] = new TranslatableMarkup('Label after: @value', [
      '@value' => $this->getSetting('multiple_entities')['label_after'],
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $entities = $this->getPurchasableEntities($items);
    if (!$entities) {
      return $element;
    }

    // Array of entities related to caching of this element.
    $cache_entities = array_merge([$items->getEntity()], $this->getPurchasableEntities($items, FALSE));
    // The lowest prized entity.
    $entity = $this->getLowestPricedEntity($entities);

    // If requested, consider multiple equal prices as a single entity price.
    $multiple = !($this->validateEqualPrices($entities) && $this->getSetting('process_equal_prices_as_single')) && $items->count() > 1;

    // Display the lowest price with the appropriate currency.
    $element = [
      '#theme' => 'commerce_from_price',
      '#label_before' => $multiple
        ? $this->getSetting('multiple_entities')['label_before']
        : $this->getSetting('single_entity')['label_before'],
      '#price' => $this->getPriceElement($entity, $cache_entities),
      '#label_after' => $multiple
        ? $this->getSetting('multiple_entities')['label_after']
        : $this->getSetting('single_entity')['label_after'],
      '#cache' => [
        'tags' => $this->getCacheTags($cache_entities),
        'contexts' => $this->getCacheContexts($cache_entities),
      ],
    ];

    return $element;
  }

  /**
   * Get published purchasable entities from the field item list.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field item list.
   * @param bool $published_only
   *   True to only return published entities.
   *
   * @return \Drupal\commerce\PurchasableEntityInterface[]
   *   The purchasable entities.
   */
  public function getPurchasableEntities(FieldItemListInterface $items, bool $published_only = TRUE): array {
    $entities = [];

    foreach ($items as $item) {
      /** @var \Drupal\commerce\PurchasableEntityInterface $entity */
      $entity = $item->get('entity')->getTarget()->getEntity();
      if (($published_only && $entity->isPublished()) || !$published_only) {
        $entities[] = $entity;
      }
    }

    return $entities;
  }

  /**
   * Check if entities have equal prices.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface[] $entities
   *   The entities to compare.
   *
   * @return bool
   *   True if all prices are equal for given entities.
   */
  public function validateEqualPrices(array $entities): bool {
    $previous_price = NULL;

    foreach ($entities as $entity) {
      $price = $this->getResolvedPrice($entity);
      if (NULL !== $previous_price && $price->getNumber() !== $previous_price->getNumber()) {
        return FALSE;
      }

      $previous_price = $price;
    }

    return TRUE;
  }

  /**
   * Get the lowest priced entity.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface[] $entities
   *   The entities to compare.
   *
   * @return \Drupal\commerce\PurchasableEntityInterface|null
   *   The entity with the lowest price or null if input was empty.
   */
  public function getLowestPricedEntity(array $entities): ?PurchasableEntityInterface {
    $lowest_priced_entity = NULL;

    foreach ($entities as $entity) {
      $price = $this->getResolvedPrice($entity);
      if ($lowest_priced_entity === NULL || $price->getNumber() < $lowest_priced_entity->getPrice()->getNumber()) {
        $lowest_priced_entity = $entity;
      }
    }

    return $lowest_priced_entity;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    $entity_type = $field_definition->getTargetEntityTypeId();
    $field_name = $field_definition->getName();

    return $entity_type === 'commerce_product' && $field_name === 'variations';
  }

  /**
   * Get resolved price of purchasable entity.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The entity to calculate the price for.
   *
   * @return \Drupal\commerce_price\Price
   *   The price.
   */
  public function getResolvedPrice(PurchasableEntityInterface $entity): Price {
    return $entity->getPrice();
  }

  /**
   * Get cache tags from entities.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
   *   The entities to collect cache tags from.
   *
   * @return array
   *   Cache tags.
   */
  public function getCacheTags(array $entities): array {
    $cache_tags = [];

    foreach ($entities as $entity) {
      $cache_tags = Cache::mergeTags($cache_tags, $entity->getCacheTags());
    }

    return $cache_tags;
  }

  /**
   * Get cache contexts from entities.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
   *   The entities to collect cache contexts from.
   *
   * @return array
   *   Cache contexts.
   */
  public function getCacheContexts(array $entities): array {
    $cache_contexts = [
      'languages:' . LanguageInterface::TYPE_INTERFACE,
      'country',
    ];

    foreach ($entities as $entity) {
      $cache_contexts = Cache::mergeContexts($cache_contexts, $entity->getCacheContexts());
    }

    return $cache_contexts;
  }

  /**
   * Get render array for price element.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The entity to build a price element for.
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $cache_entities
   *   The entities related to cache.
   *
   * @return array
   *   Price element render array.
   */
  abstract public function getPriceElement(PurchasableEntityInterface $entity, array $cache_entities): array;

}
